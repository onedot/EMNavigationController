//
//  FirstViewController.m
//  EMultiNavigation
//
//  Created by EasonWang on 13-11-5.
//  Copyright (c) 2013年 EasonWang. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navTitle = @"First";
    
    // 设置navigationBar左侧的item
    [self setLeftItemWithTarget:self action:@selector(testLeftItem) title:@"left"];
    
    // 本地化string
    _labelText.text = NSLocalizedString(@"label", nil);
    
    // 本地化image
    UIImage *image = [UIImage imageNamed:NSLocalizedStringFromTable(@"image.png", nil, nil)];
    [_imageView setImage:image];

    // ActivityIndicatorView
    [self.loadingView startAnimating];
    
    // 3s 后关闭活动指示器
    [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(stopLoadingView) userInfo:nil repeats:NO];
}
-(void)stopLoadingView
{
    [self.loadingView stopAnimating];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)clickButton:(id)sender
{
    SecondViewController *vc = [[SecondViewController alloc]initWithNibName:@"SecondViewController" bundle:nil];
    [NavigationController pushViewController:vc animated:YES];
}

-(void)testLeftItem
{
    NSLog(@"----------leftItem---------");
}

@end
